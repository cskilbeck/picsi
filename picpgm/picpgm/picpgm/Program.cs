﻿// TODO (chs): support different PIC burning protocols
// TODO (chs): look for data at address 2100 (eg 4200) and burn it to the eeprom...
// TODO (chs): option for baud rate
// TODO (chs): convert to C++ and make it cross platform

// DONE (chs): output from read should be a hex file (but how to determine the end of the code? and the data?)
// DONE (chs): progress bar on write
// DONE (chs): allow multiple commands (eg burn this hex file then reset it)
// DONE (chs): allow more than a single word to be sent at once (eg a whole hex file record)
// DONE (chs): get/set config word
// DONE (chs): look for data at address 2007 (eg 400e) and write it to the config word
// DONE (chs): read chip ID
// DONE (chs): verbosity levels? (settled for debug mode)

// NOPE (chs): build memory map from HEX file then send it in contiguous blocks (PIC doesn't seem to like writing >16 at a time)
// NOPE (chs): switch PIC on/off only when necessary (eg at the start and end) (seems you kind of have to, most of the time)

using System;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

namespace picpgm
{
    class PIC
    {
        public string Name;
        public ushort ID;

        public PIC(string name, ushort id)
        {
            Name = name;
            ID = id;
        }
    }

    //////////////////////////////////////////////////////////////////////
    // main
    
    class Program
    {
        static List<PIC> PICs = new List<PIC>
        {
            new PIC("12F629",  0x0F80),
            new PIC("12F675",  0x0FC0),
            new PIC("16F630",  0x10C0),
            new PIC("16F676",  0x10E0),
            new PIC("16F84a",  0x0560),
            new PIC("16F87",   0x0720),
            new PIC("16F88",   0x0760),
            new PIC("16F627",  0x07A0),
            new PIC("16F627a", 0x1040),
            new PIC("16F628",  0x07C0),
            new PIC("16F628a", 0x1060),
            new PIC("16F648a", 0x1100),
            new PIC("16F882",  0x2000),
            new PIC("16F883",  0x2020),
            new PIC("16F884",  0x2040),
            new PIC("16F886",  0x2060),
            new PIC("16F887",  0x0208),
            new PIC("16F688",  0x1180),
            new PIC("16F684",  0x1080),
        };

        public static UInt16[] GetConfig(byte[] from)
        {
            UInt16[] data = new UInt16[6];
            for (int i = 0; i < 6; ++i)
            {
                data[i] = (UInt16)((UInt16)from[i * 2] | ((UInt16)(from[i * 2 + 1]) << 8));
            }
            return data;
        }

        static PIC FindPic(int id)
        {
            return PICs.Find(pic => pic.ID == id);
        }

        static string GetPICName(int id)
        {
            PIC p = FindPic(id);
            return (p != null) ? p.Name : ".UNKNOWN";
        }

        static Burner burner;

        static Options options;

        // if address < 2000, write to program memory
        // if address > 4000, write to data/config/eeprom?

        public static void WriteHexFile()
        {
            HexFile r = new HexFile(options.InputFile);
            r.Verify();
            burner.Command(CMD.programmingMode);
            burner.Command(CMD.erase);
            burner.Command(CMD.programmingMode);

            int plen = 50;
            int progress = 0;
            int printed = 0;

            // all sorts of dodgy assumptions in here about the order and linearity of chunks in the hex file...

            while (r.ReadNext() > 0)
            {
                switch (r.Type)
                {
                    // DATA
                    case 0x00:
                        if (r.Address < 0x2000)
                        {
                            // TODO (chs): check that the addresses are linear (or better still, glom all memory sections together and do them as single blobs)
                            byte[] buffer = r.Payload;
                            burner.Command(CMD.loadCodeBlock, buffer);
                            progress += r.Data.Length;
                            // how many to print?
                            int bar = (progress * (plen + 1) - 1) / r.CodeSize;
                            Print.Line(new String('#', bar - printed));
                            printed = bar;
                        }
                        else if (r.Address == 0x400e && r.Data.Length == 2)
                        {
                            // it's the config word...
                            // assert(r.Data.Length == 2)
                            // check if LVP bit is clear, warn and confirm if so...
                            Print.Debug("\nWriting config 0x{0:x4}\n", (int)r.Data[0] + ((int)r.Data[1] << 8));
                            byte[] buffer = new byte[4];
                            buffer[0] = 2;
                            buffer[1] = 0;
                            buffer[2] = r.Data[0];
                            buffer[3] = r.Data[1];
                            burner.Command(CMD.writeConfig, buffer);
                        }
                        break;
                }
            }
            Print.Line("\n");
            burner.Command(CMD.off);
        }

        //int id = (((ushort)from[1] << 8) | from[0]) & 0x3FE0;
        public static void ReadIt()
        {
            // get the device ID and config bits
            burner.Command(CMD.programmingMode);
            byte[] config = burner.Command(CMD.readConfig);

            UInt16[] cfg = GetConfig(config);

            int id = (((ushort)config[9] << 8) | config[8]) & 0x3FE0;

            Print.Debug("DEVICE ID: 0x{0:X4}\n", id);
            Print.Debug("CONFIG: 0x{0:X4}\n", config[2] + (config[3] << 8));

            // check if we know what it is
            PIC pic = FindPic(id);
            if (pic == null)
            {
                throw new Exception("Unidentified PIC, don't know how to read it");
            }

            // reset PC to 0
            burner.Command(CMD.programmingMode);

            // start the HEX file output
            Console.WriteLine(":020000040000FA");

            // read the code
            int addr = 0;
            while (addr < 2048)    // TODO (chs): memory ranges for different PICs
            {
                // read 16 bytes
                byte[] code = burner.Command(CMD.readCode, new byte[] { 0x10, 0 });

                // check if we've reached empty space (16 x 0x3fff)
                if (!options.ReadAll)
                {
                    bool done = true;
                    for (int i = 0; i < code.Length / 2; ++i)
                    {
                        if (code[i * 2 + 1] != 0x3f || code[i * 2] != 0xff)
                        {
                            done = false;
                            break;
                        }
                    }
                    if (done)
                    {
                        break;
                    }
                }
                // print the hex file line
                Console.WriteLine(HexFile.Line(code, addr));

                addr += code.Length;
            }

            // reset the PIC again
            burner.Command(CMD.programmingMode);

            // read EEPROM data
            addr = 0x2100;
            while (addr < 0x2180)
            {
                byte[] data = burner.Command(CMD.readData, new byte[] { 0x10, 0 });

                if (!options.ReadAll)
                {
                    bool done = true;
                    for (int i = 0; i < data.Length; ++i)
                    {
                        if (data[i] != 0xff)
                        {
                            done = false;
                            break;
                        }
                    }
                    if (done)
                    {
                        break;
                    }
                }
                Console.WriteLine(HexFile.Line(data, addr));
                addr += data.Length;
            }
            // add the config at the end
            Console.WriteLine(HexFile.Line(new byte[] { config[10], config[11] }, 0x400e));
            Console.WriteLine(":00000001FF");
            burner.Command(CMD.off);
        }

        public static void Config()
        {
            int config = Convert.ToInt32(options.ConfigWord, 16);
            byte[] cfg = new byte[] { 2, 0, (byte)config, (byte)(config >> 8) };
            burner.Command(CMD.writeConfig, cfg);
            burner.Command(CMD.off);
        }

        public static void EraseIt()
        {
            burner.Command(CMD.programmingMode);
            burner.Command(CMD.erase);
            burner.Command(CMD.off);
            Print.Line("OK\n");
        }

        public static void On()
        {
            burner.Command(CMD.execute);
        }

        public static void Off()
        {
            burner.Command(CMD.off);
        }

        public static void ResetConfig()
        {
            burner.Command(CMD.programmingMode);
            burner.Command(CMD.resetConfig);
            burner.Command(CMD.off);
        }

        public static void Verify()
        {
            burner.Command(CMD.programmingMode);

            StringBuilder error = new StringBuilder();
            HexFile r = new HexFile(options.InputFile);
            r.Verify();
            int addr = 0;
            Print.Debug(":020000040000FA");
            while (r.ReadNext() > 0)
            {
                switch (r.Type)
                {
                    // DATA
                    case 0x00:
                        if (r.Address < 0x2000)
                        {
                            // TODO (chs): check that the addresses are linear (or better still, glom all memory sections together and do them as single blobs)
                            byte[] from = burner.Command(CMD.readCode, new byte[] { (byte)r.Data.Length, 0 });  // should return data.Length bytes
                            // compare from with r.Data[]
                            if (r.Data.Length != from.Length)
                            {
                                throw new BurnerException(string.Format("Bad length back at {0:x4}, got {1}, expected {2}", r.Address, from.Length, r.Data.Length));
                            }
                            Print.Debug(":{0:X2}{1:X4}00", from.Length, addr);
                            for (int i = 0; i < r.Data.Length; ++i)
                            {
                                if (r.Data[i] != from[i])
                                {
                                    throw new BurnerException(string.Format("Bad byte back at {0:X4}, expected {1:X2}, got {2:X2}", r.Address + i, r.Data[i], from[i]));
                                }
                                if (i % 2 == 0)
                                {
                                    Print.Debug("{0:X2}{1:X2}", from[i], from[i + 1]);
                                }
                            }
                            Print.Debug("CC\n");
                            addr += from.Length;
                        }
                        else if (r.Address == 0x400e && r.Data.Length == 2)
                        {
                            byte[] from = burner.Command(CMD.readConfig);   // should return 12 bytes 4 16 bit serial #s, 1 16 bit device ID, 1 16 bit config word
                            if (from.Length != 12)
                            {
                                throw new BurnerException(string.Format("Bad config length back, expected 12, got {0}", from.Length));
                            }
                            addr = 0x400e;
                            Print.Debug(":{0:X2}{1:X4}00", 2, addr);
                            int config_got = (int)(from[10] + ((int)from[11] << 8));
                            int config_hex = (int)(r.Data[0] + ((int)r.Data[1] << 8));
                            if(config_got != config_hex)
                            { 
                                error.Append(string.Format("Bad config, expected {0:X4}, got {1:X4}\n", config_hex, config_got));
                            }
                            Print.Debug("CC\n");
                        }
                        break;
                }
            }
            Print.Debug(":00000001FF");
            burner.Command(CMD.off);
            if (error.Length > 0)
            {
                throw new BurnerException(error.ToString());
            }
        }

        public static void ResetPIC()
        {
            burner.Command(CMD.execute);
        }

        public static void Test()
        {
            burner.Command(CMD.programmingMode);
        }

        public static void Ping()
        {
            burner.Command(CMD.programmingMode);
            byte[] from = burner.Command(CMD.readConfig);
            burner.Command(CMD.off);
            if (from.Length != 12)
            {
                throw new BurnerException(string.Format("Bad config length back, expected 4, got {0}", from.Length));
            }

            UInt16[] data = GetConfig(from);
            int id = data[4] & 0x3FE0;
            int config = data[5];
            Console.WriteLine("DEVICE ID: 0x{0:x4} = PIC{1}", id, GetPICName(id));
            Console.WriteLine("SERIAL: {0:x2}{1:x2}{2:x2}{3:x2}", data[0], data[1], data[2], data[3]);
            Console.WriteLine("CONFIG: 0x{0:x4}", config);
        }

        public static void Flush()
        {
            burner.Flush();
        }

        static int Main(string[] args)
        {
            int rc = 0;
            try
            {
                options = Options.ParseArguments(args);

                burner = new Burner(options.ComPort);

                foreach (string cmd in options.Commands)
                {
                    Options.Command c = Options.FindCommand(cmd);
                    if (c == null)
                    {
                        throw new OptionsException("Unknown command '" + cmd + "', commands are " + Options.CommandList());
                    }
                    if (c.NeedsFile && options.InputFile == null)
                    {
                        throw new OptionsException("Command '" + cmd + "' needs an input file");
                    }
                    Print.Line("{0}{1}\n", cmd, new String('-', 50 - cmd.Length));
                    c.Function();
                }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
                if(burner != null)
                {
                    burner.Flush();
                }
                rc = 1;
                
            }
            finally
            {
            }
            if (Debugger.IsAttached)
            {
                Console.ReadLine();
            }
            return rc;
        }
    }
}
