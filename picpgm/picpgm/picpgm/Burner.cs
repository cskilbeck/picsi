﻿using System;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Collections.Generic;
using System.Management;
using System.Threading;

namespace picpgm
{
    class BurnerException: Exception
    {
        public BurnerException(string m)
            : base(m)
        {
        }
    }

    class Burner
    {
        SerialPort serialPort;

        public static string AutodetectArduinoPort()
        {
            ManagementScope connectionScope = new ManagementScope();
            SelectQuery serialQuery = new SelectQuery("SELECT * FROM Win32_SerialPort");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(connectionScope, serialQuery);

            try
            {
                foreach (ManagementObject item in searcher.Get())
                {
                    string desc = item["Description"].ToString();
                    string deviceId = item["DeviceID"].ToString();

                    if (desc.Contains("Arduino"))
                    {
                        Print.Debug("Detected Arduino on " + deviceId + "\n");
                        return deviceId;
                    }
                }
            }
            catch (ManagementException)
            {
                throw new BurnerException("Can't enumerate serial ports!?");
            }

            return null;
        }

        public Burner(string portName)
        {
            string port = null;

            if (portName != null && portName.Length > 0)
            {
                portName = portName.ToUpper();
                if(Array.IndexOf(SerialPort.GetPortNames(), portName) >= 0)
                {
                    port = portName;
                }
                else
                {
                    throw new BurnerException("SerialPort " + portName + " not found");
                }
            }
            else
            {
                port = AutodetectArduinoPort();
                if(port == null)
                {
                    throw new BurnerException("Can't find an Arduino, try specifying the port with -p COMx");
                }
            }

            try
            {
                serialPort = new SerialPort(port, 57600, Parity.None, 8);
                serialPort.Encoding = Encoding.GetEncoding(28591);              // don't mess with the bytes
                serialPort.ReadTimeout = 5000;
                serialPort.WriteTimeout = 5000;
                serialPort.Open();
            }
            catch (IOException e)
            {
                throw new BurnerException("Error opening " + port + ": " + e.Message);
            }

            // send it a ping to check it's running PICSI
            //Command(CMD.ping);

            Print.Debug("Opened {0}\n", serialPort.PortName);
        }

        public void Close()
        {
            if (serialPort != null)
            {
                Print.Line("Closing " + serialPort.PortName);
                serialPort.Close();
                serialPort = null;
            }
        }

        public void Flush()
        {
            if(serialPort != null)
            {
                serialPort.ReadTimeout = 500;
                serialPort.ReadExisting();
            }
            else
            {
                throw new BurnerException("No serial port to flush!?");
            }
        }

        public byte[] Command(CMD cmd, byte[] arg = null)
        {
            try
            {
                Print.Debug("{0}:", cmd);
                byte[] b = { (byte)cmd };
                serialPort.Write(b, 0, 1);
                if (arg != null && arg.Length > 0)
                {
                    serialPort.Write(arg, 0, arg.Length);
                    foreach(byte a in arg)
                    {
                        Print.Debug("{0:X2}", a);
                    }
                }
                Print.Debug(" >> ");
                List<byte> s = new List<byte>();
                int cmdR = serialPort.ReadByte();
                int lenR = serialPort.ReadByte() + ((int)serialPort.ReadByte() << 8);
                Print.Debug("CMD:0x{0:x2}=0x{1:x2}, 0x{2:x2}", (int)cmd, cmdR, lenR);
                if ((byte)cmd != (cmdR ^ 0xff))
                {
                    throw new BurnerException(String.Format("Bad response back: 0x{0:x2}", cmdR));
                }
                else if (lenR != 0)
                {
                    for (int i = 0; i < lenR; ++i)
                    {
                        int d = serialPort.ReadByte();
                        Print.Debug(" {0:x2}", d);
                        s.Add((byte)d);
                    }
                }
                Print.Debug("\n");
                return s.ToArray();
            }
            catch(TimeoutException e)
            {
                Console.WriteLine(e.Message);
                throw new BurnerException("COM port timeout - is it running PICSI?");
            }
        }
    }
}