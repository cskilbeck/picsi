﻿#if __cplusplus_cli
namespace picpgm {
    public
#endif

enum CMD : int {
    invalid = 0,
    programmingMode,    // reset PIC into program (PGM=1) mode
    ping,               // check burner is available
    off,                // switch off PIC
    erase,              // erase the code
    readData,           // read N bytes of data
    readProgram,        // read a word of program
    loadCodeBlock,      // write a block of program
    loadDataBlock,      // write a block of data
    loadCodeWord,       // load a word into program latch
    loadDataWord,       // load a word into data latch
    programWord,        // write latched word
    eraseProgramWord,   // erase & write latched word
    readCode,           // read 1st ? words of program
    readConfig,         // read config
    resetConfig,        // reset config (erases whole PIC & removes code protection)
    incrementAddress,   // increment PC
    writeConfig,        // write config word
    flush,              // flush serial from burner
    execute             // reset PIC into execute (PGM=0) mode
};

#if __cplusplus_cli
}
#endif
