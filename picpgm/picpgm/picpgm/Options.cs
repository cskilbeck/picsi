﻿using System;
using CommandLine;
using CommandLine.Text;
using System.Collections.Generic;
using System.Text;

namespace picpgm
{
    //////////////////////////////////////////////////////////////////////

    class OptionsException : Exception
    {
        public OptionsException(string m)
            : base(m)
        {
        }
    }

    class Options
    {
        public class Command
        {
            public delegate void FunctionDelegate();

            public string Name;
            public bool NeedsFile;
            public FunctionDelegate Function;
            public string Help;

            public Command(string name, bool needsFile, FunctionDelegate function, string help)
            {
                Name = name;
                NeedsFile = needsFile;
                Function = function;
                Help = help;
            }
        }

        static string write_Help = "Burns the hexfile (specified with -f foo.hex) to the PIC";
        static string read_Help = "Reads the contents of the PIC (just the first few bytes of code and data, currently)";
        static string erase_Help = "Erases the code and data sections of the PIC";
        static string verify_Help = "Compares contents of the hex file (-f foo.hex) with the contents of the PIC. If they differ, the return value will be non-zero";
        static string config_Help = "Write config (specified with -c XXXX)";
        static string execute_Help = "Executes the code on the PIC";
        static string ping_Help = "Reads the device ID and config bits from the PIC";
        static string on_Help = "Switches on the PIC";
        static string off_Help = "Switches off the PIC";
        static string flush_Help = "Flush serial traffic in case of sync issues";
        static string testpgm_Help = "Set VPP high and leave it there (use 'off' to turn it off again)";
        static string factoryreset_Help = "Factory reset the PIC (including config bits)";
        static string commands_Help = "Show this help text for each command";

        public static Command[] commands =
        {
            new Command("write", true, Program.WriteHexFile, write_Help),               // burn hexfile to pic
            new Command("read", false, Program.ReadIt, read_Help),                      // read data from pic
            new Command("erase", false, Program.EraseIt, erase_Help),                   // erase the pic
            new Command("verify", true, Program.Verify, verify_Help),                   // verify hexfile is on pic
            new Command("config", false, Program.Config, config_Help),                  // write config word
            new Command("execute", false, Program.ResetPIC, execute_Help),              // run the pic
            new Command("ping", false, Program.Ping, ping_Help),                        // ping the burner
            new Command("on", false, Program.On, on_Help),                              // switch on the pic
            new Command("off", false, Program.Off, off_Help),                           // switch off the pic
            new Command("flush", false, Program.Flush, flush_Help),                     // try to flush serial traffic?
            new Command("testpgm", false, Program.Test, testpgm_Help),                  // switch on VPP and quit
            new Command("factoryreset", false, Program.ResetConfig, factoryreset_Help),  // full pic reset
            new Command("commands", false, Options.CommandHelp, commands_Help)
        };

        public static void CommandHelp()
        {
            Console.WriteLine("Command help:\n");
            foreach(Command cmd in commands)
            {
                Console.WriteLine(cmd.Name + " - " + cmd.Help);
            }
            Console.WriteLine();
        }

        public static Command FindCommand(string n)
        {
            return Array.Find<Command>(commands, s => n == s.Name);
        }

        public static string CommandList()
        {
            StringBuilder b = new StringBuilder();
            foreach(Command cmd in commands)
            {
                b.Append(cmd.Name + " ");
            }
            return b.ToString();
        }

        [Option('f', "file", HelpText = "Input hex file")]
        public string InputFile { get; set; }

        [Option('v', "verbose", HelpText = "Verbose output")]
        public bool Verbose { get; set; }

        [Option('p', "port", HelpText = "COM port to use, eg COM3. If not specified, tries to detect an Arduino")]
        public string ComPort { get; set; }

        [Option("debug", HelpText = "Lots of debug text")]
        public bool Debug { get; set; }

        [Option('a', "all", HelpText = "When reading, don't detect end of code/data, read all of it")]
        public bool ReadAll { get; set; }

        [Option('c', "config", HelpText = "Specify config (in hex, 4 digits) for config command")]
        public string ConfigWord { get; set; }

        [ValueList(typeof(List<string>))]
        public IList<string> Commands { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption(HelpText = "Display this help")]
        public string GetUsage()
        {
            string s = "Usage: picpgm [options] [commands]\n\n";
            s += HelpText.AutoBuild(this);
            s += "\nCommands: " + CommandList();
            return s;
        }

        public string ShortUsage()
        {
            return "Usage: picpgm [options] command [command...]\n try 'picpgm -h' for help and 'picpgm commands' for details";
        }

        public static Options ParseArguments(string[] args)
        {
            Parser parser = new Parser();
            Options options = new Options();

            bool success = parser.ParseArguments(args, options);

            if (args.Length == 0)
            {
                throw new OptionsException(options.ShortUsage());
            }

            if (!success || args.Length == 0)
            {
                throw new OptionsException(options.GetUsage());
            }

            Print.verbose = options.Verbose;
            Print.debug = options.Debug;

            if(options.Commands.Count == 0)
            {
                Console.Error.WriteLine(options.GetUsage());
                throw new OptionsException("Nothing to do, commands are " + CommandList());
            }

            return options;
        }

    }
}