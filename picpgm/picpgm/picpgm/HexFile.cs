﻿using System;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;

//////////////////////////////////////////////////////////////////////
// for reading records from the hex file

// :LLAAAATTDDDDCC

// LL           length byte     how many data bytes there are, if LL == 0, you're done (EOF)
// AAAA         address word    address to put the data bytes
// TT           type byte       should it go into program (00), data or config memory
// DDDDDDD      data            [LL] bytes of data
// CC           checksum        should be (((LL + AA[0:7] + AA[8:15] + TT + [DD...]) & 0xff) ^ 0xff) + 1

// :020000040000FA
// :1000000007309F0083160030860085008312FF3082
// :10001000850086000000000000308500860007286B
// :040020000034003474
// :02400E00983FD9
// :00000001FF

namespace picpgm
{
    public class Section : IComparable<Section>
    {
        public int Address;
        public List<byte> Data;

        public Section(int address, byte[] data)
        {
            Address = address;
            Data = new List<byte>(data);
        }

        public int Length
        {
            get
            {
                return Data.Count;
            }
        }

        int IComparable<Section>.CompareTo(Section other)
        {
            return Address - other.Address;
        }

        public int EndAddress
        {
            get
            {
                return Address + Length;
            }
        }

        public void Append(byte[] data)
        {
            Data.AddRange(data);
        }

        public byte[] Payload()
        {
            byte[] p = new byte[Length + 2];
            // 2 bytes of length
            p[0] = (byte)(Length & 0xff);
            p[1] = (byte)(Length >> 8);
            // then the data
            for (int i = 0; i < Length; ++i)
            {
                p[i + 2] = Data[i];
            }
            return p;
        }
    }

    public class HexFileException: Exception
    {
        public HexFileException(string m)
            : base(m)
        {
        }
    }

    public class HexFile
    {
        int mLine;
        int mCount;
        int mAddress;
        int mBaseAddress;
        int mType;
        int mTotalCodeBytes;
        byte[] mData;
        int mChecksum;
        FileStream f;
        int next;
        int prev;

        LinkedList<Section> mSections = new LinkedList<Section>();

        public int Count { get { return mCount; } }
        public int Address { get { return mAddress; } }
        public int Type { get { return mType; } }
        public int CodeSize { get { return mTotalCodeBytes; } }
        public byte[] Data { get { return mData; } }

        public HexFile(string filename)
        {
            mLine = 0;
            // open the hex file
            f = new FileStream(filename, FileMode.Open, FileAccess.Read);
            next = '\n';
        }

        int NextByte()
        {
            prev = next;
            next = f.ReadByte();
            if(next < 0)
            {
                throw new HexFileException("Unexpected EOF (expected something)");
            }
            Print.Debug("{0}", (char)next);
            return next;
        }

        int Hex2Int(int ch)
        {
            return
                (ch >= '0' && ch <= '9') ? (ch - '0' + 0) :
                (ch >= 'A' && ch <= 'F') ? (ch - 'A' + 10) :
                (ch >= 'a' && ch <= 'f') ? (ch - 'a' + 10) : -1;
        }

        int ReadNibbles(int nibbles)
        {
            int r = 0;
            for (int i = 0; i < nibbles; ++i)
            {
                int n = Hex2Int(NextByte());
                if (n < 0)
                {
                    return -1;
                }
                r = (r << 4) | n;
            }
            return r;
        }

        int ReadByte()
        {
            return ReadNibbles(2);
        }

        int ReadWord()
        {
            return ReadNibbles(4);
        }

        byte[] ReadBytes(int count)
        {
            byte[] b = new byte[count];
            for (int i = 0; i < count; ++i)
            {
                int r = ReadByte();
                if (r < 0)
                {
                    return null;
                }
                b[i] = (byte)r;
            }
            return b;
        }

        static int Sum(byte[] data)
        {
            int sum = 0;
            foreach (byte b in data)
            {
                sum += b;
            }
            return sum;
        }

        public LinkedList<Section> Sections
        {
            get
            {
                return mSections;
            }
        }

        public int ReadAll()
        {
            mTotalCodeBytes = 0;
            while(true)
            {
                int count = ReadNext();
                if(count == 0)
                {
                    break;
                }
                if(mType == 0x00)
                {
                    mTotalCodeBytes += mCount;
                    mSections.AddLast(new Section(mAddress, mData));
                }
            }

            // now there are just contiguous memory sections, so sending to the PIC will be simpler/faster
            return mSections.Count;
        }

        public byte[] Payload
        {
            get
            {
                byte[] p = new byte[mCount + 2];
                // 2 bytes of length
                p[0] = (byte)(mCount & 0xff);
                p[1] = (byte)(mCount >> 8);
                // then the data
                for (int i = 0; i < mCount; ++i)
                {
                    p[i + 2] = mData[i];
                }
                return p;
            }
        }

        public int ReadNext()
        {
            while (true)
            {
                NextByte();
                if (next == ':' && prev == '\n')
                {
                    break;
                }
            }
            ++mLine;
            mCount = ReadByte();
            mAddress = ReadWord() + mBaseAddress;
            mType = ReadByte();
            mData = ReadBytes(mCount);
            mChecksum = ReadByte();
            int check = 0xff & ((((mCount + (mAddress & 0xff) + (mAddress >> 8) + mType + Sum(mData)) & 0xff) ^ 0xff) + 1);
            if (mChecksum != check)
            {
                throw new HexFileException("Checksum error at line " + mLine + ", expected " + check + ", got " + mChecksum);
            }
            Print.Debug("\nCount: 0x{0:x2}, Addr: 0x{1:x4}, Type: 0x{2:x2}, Data: {3} bytes, Checksum: 0x{4:x2}\n",
                mCount, mAddress, mType, mData.Length, mChecksum);
            if(mType == 0x02)
            {
                mBaseAddress = (mData[0] << 8 | mData[1]);
            }
            return mCount;
        }

        // throw if hexfile bad
        public bool Verify()
        {
            ReadAll();
            f.Position = 0;
            return true;
        }

        // make a line of a hexfile

        public static string Line(byte[] data, int addr)
        {
            int count = data.Length;
            int type = 0;
            int checksum = 0xff & ((((count + (addr & 0xff) + (addr >> 8) + type + Sum(data)) & 0xff) ^ 0xff) + 1);
            StringBuilder s = new StringBuilder();
            s.Append(string.Format(":{0:X2}{1:X4}{2:X2}", count, addr, type));
            foreach(byte b in data)
            {
                s.Append(string.Format("{0:X2}", b));
            }
            s.Append(string.Format("{0:X2}", checksum));
            return s.ToString();
        }
    }
}
