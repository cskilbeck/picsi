﻿using System;

namespace picpgm
{
    //////////////////////////////////////////////////////////////////////
    // turn off and onable printer

    public static class Print
    {
        public static bool verbose;
        public static bool debug;

        public static void Line(string fmt)
        {
            if (verbose)
            {
                Console.Write(fmt);
            }
        }

        public static void ToScreen(string fmt)
        {
            if (verbose && !Console.IsOutputRedirected)
            {
                Console.Write(fmt);
            }
        }

        public static void Line(string fmt, params object[] args)
        {
            if (verbose)
            {
                Console.Write(fmt, args);
            }
        }

        public static void ToScreen(string fmt, params object[] args)
        {
            if (verbose && !Console.IsOutputRedirected)
            {
                Console.Write(fmt, args);
            }
        }

        public static void Debug(string fmt, params object[] args)
        {
            if (debug)
            {
                Console.Write(fmt, args);
            }
        }
    }
}