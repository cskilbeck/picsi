#include "Pins.h"

static uint8_t pulse = 0;
static uint16_t pulsedir = 0;
static uint16_t pulsevel = 2;

void PulseLed()
{
    digitalWrite(VPP_LED, pulse++ > (pulsedir >> 8));
    if(!(pulsedir += pulsevel)) {
        pulsevel = -pulsevel;
    }
}
