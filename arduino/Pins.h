#ifndef __PINS_H__
#define __PINS_H__

// Arduino -> ICSP connections

#define DATA    8
#define CLOCK   9
#define VCC     10
#define VPP     11
#define VPP_LED 12
#define PWR_LED 13

#endif
