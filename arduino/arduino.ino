//////////////////////////////////////////////////////////////////////
// TODO (chs): sort out factory reset
// TODO (chs): sort out different EEPROM modes
// TODO (chs): make read output a hex file
//
// DONE (chs): make a common include file for the command definitions shared with the PC code
//////////////////////////////////////////////////////////////////////

// Delays, protocols... (mini command language?)

#include <Arduino.h>
#include "Pins.h"
#include "Pulse.h"
#include "Port.h"
#include "../picpgm/picpgm/picpgm/cmds.cs"  // shared enum of command list

//////////////////////////////////////////////////////////////////////
// PIC ICSP commands

#define LOAD_CONFIG                     B000000
#define LOAD_DATA_PROGRAM               B000010
#define LOAD_DATA_DATA                  B000011
#define INCREMENT_ADDRESS               B000110
#define READ_DATA_PROGRAM               B000100
#define READ_DATA_DATA                  B000101

//#define BEGIN_ERASE_PROGRAMMING         B001000
//#define BEGIN_PROGRAMMING_ONLY          B011000

#define BEGIN_ERASE_PROGRAMMING         B001000 // TODO (chs): these need to be different depending on the PIC
#define BEGIN_PROGRAMMING_ONLY          B001000

#define BULK_ERASE_PROGRAM              B001001
#define BULK_ERASE_DATA                 B001011
#define BULK_ERASE_SETUP1               B000001
#define BULK_ERASE_SETUP2               B000111

//////////////////////////////////////////////////////////////////////
// Delays (from the datasheet, mostly, some fudging)

#define delay_powerUp()                 delay(50)   // after switching on VPP
#define delay_powerDown()               delay(90)  // after switching off VCC
#define delay_Tppdp()                   delay(10)   // between switching on VCC and VPP
#define delay_Thld0()                   delayMicroseconds(5)
//#define delay_Tset1()                   delayMicroseconds(3)
#define delay_Tset1()                   delayMicroseconds(5)
#define delay_Thld1()                   delayMicroseconds(5)
#define delay_Tdly2()                   delayMicroseconds(5)
#define delay_Tera()                    delay(8)
#define delay_Tprog()                   delay(5)
#define delay_TDprog()                  delay(6)
// #define delay_TeraTprog()               delay(15)   // datasheet says 13 but 15 seems more reliable
// #define delay_TeraTprogBulk()           delay(20)   // datasheet says 13 but needs to be 20
#define delay_TeraTprog()               delay(20)   // datasheet says 13 but 15 seems more reliable
#define delay_TeraTprogBulk()           delay(20)   // datasheet says 13 but needs to be 20

//////////////////////////////////////////////////////////////////////
// one time init

void setup() {

    // all pins to output

    digitalWrite(VPP, HIGH);
    digitalWrite(VCC, HIGH);
    digitalWrite(CLOCK, LOW);
    digitalWrite(DATA, LOW);
    digitalWrite(VPP_LED, LOW);
    digitalWrite(PWR_LED, LOW);

    pinMode(VPP, OUTPUT);
    pinMode(VCC, OUTPUT);
    pinMode(DATA, OUTPUT);
    pinMode(CLOCK, OUTPUT);
    pinMode(PWR_LED, OUTPUT);
    pinMode(VPP_LED, OUTPUT);

    digitalWrite(VPP_LED, HIGH);
    delay(100);
    digitalWrite(VPP_LED, LOW);
    delay(100);

    // setup serial port

    Serial.begin(57600);
}

//////////////////////////////////////////////////////////////////////
// Control VPP & VCC

void VPP_On()
{
    digitalWrite(VPP, LOW);
}

void VPP_Off()
{
    digitalWrite(VPP, HIGH);
}

void VCC_On()
{
    digitalWrite(PWR_LED, HIGH);
    digitalWrite(VCC, LOW);
}

void VCC_Off()
{
    digitalWrite(PWR_LED, LOW);
    digitalWrite(VCC, HIGH);
}

//////////////////////////////////////////////////////////////////////
// switch off the PIC

void SwitchOffPIC()
{
    VPP_Off();
    VCC_Off();
    delay_powerDown();
    digitalWrite(CLOCK, LOW);
    digitalWrite(DATA, LOW);
    pinMode(DATA, OUTPUT);
    pinMode(CLOCK, OUTPUT);

}

//////////////////////////////////////////////////////////////////////
// get pic ready for programming

void EnterProgrammingMode() {

    // all off except VPP
    SwitchOffPIC();

    // power on, VPP high simultaneously, seems to work
    VCC_On();

    // VARIABLE, depending on PIC
    delay_Tppdp();

    VPP_On();

    delay_powerUp();
}

//////////////////////////////////////////////////////////////////////
// run the code on the pic

void RunPIC() {

    // everything off
    SwitchOffPIC();

    // switch on the pic, not VPP though
    VCC_On();
}

//////////////////////////////////////////////////////////////////////
// command responses

//////////////////////////////////////////////////////////////////////
// ACK, 0 - no data back, just ack

void Reply0(byte command) {
    Port::PutByte(command ^ 0xff);
    Port::PutWord(0);
}

//////////////////////////////////////////////////////////////////////
// ACK, 2, UINT16 - 16bit word reply

void Reply2(byte command, uint16_t data) {
    Port::PutByte(command ^ 0xff);
    Port::PutWord(2);
    Port::PutWord(data);
}

//////////////////////////////////////////////////////////////////////
// ACK, N, N bytes - N words of data in the reply

void ReplyN(byte command, uint16_t len) {
    Port::PutByte(command ^ 0xff);
    Port::PutWord(len);
}

//////////////////////////////////////////////////////////////////////
// main loop

void loop() {
    uint16_t got, config;

    // pulsing LED when idle, in the time honoured tradition
    if(!Serial.available()) {
        PulseLed();
    }
    else {
        byte command = Serial.read();

        switch (command) {

            case ping:
                Reply0(command);
                break;

            case off:
                SwitchOffPIC();
                Reply0(command);
                break;

            case programmingMode:
                EnterProgrammingMode();
                Reply0(command);
                break;

            case erase:
                FullEraseProgramMemory();   // TODO (chs) work out why it needs erasing twice (for some PICs)
                EnterProgrammingMode();
                for(int i=0; i<4; ++i)
                {
                    LoadDataForProgramMemory(0x3fff);   // flush latches
                    IncrementAddress();
                }
                Reply0(command);
                break;

            case readProgram:
                Reply2(command, ReadDataFromProgramMemory());
                break;

            case loadCodeBlock:
                got = Port::GetWord();
                for(int i=0; i<got/2; ++i) {
                    uint16_t wrd = Port::GetWord();
                    //LoadDataForProgramMemory(0x1234);
                    LoadDataForProgramMemory(wrd);
                    BeginProgrammingOnlyCycle();
                    IncrementAddress();
                }
                Reply2(command, got);
                break;

            case loadCodeWord:
                got = Port::GetWord();
                LoadDataForProgramMemory(got);
                Reply2(command, got);
                break;

            case loadDataWord:
                got = Port::GetWord();
                LoadDataForDataMemory(got);
                Reply2(command, got);
                break;

            case programWord:
                BeginProgrammingOnlyCycle();
                delay_Tprog();
//                WriteWord14(0);
                Reply0(command);
                break;

            case eraseProgramWord:
                BeginEraseProgrammingCycle();
                Reply0(command);
                break;

            case readCode:
                got = Port::GetWord();    // how many to read
                LoadDataForProgramMemory(0x3fff);
                ReplyN(command, got);
                for(int i=0; i<got / 2; ++i) {
                    Port::PutWord(ReadDataFromProgramMemory());
                    IncrementAddress();
                }
                break;

            case readData:
                got = Port::GetWord();
                LoadDataForDataMemory(0);
                ReplyN(command, got);
                for(int i=0; i<got; ++i) {
                    Port::PutByte(ReadDataFromDataMemory());
                    IncrementAddress();
                }
                break;

            case readConfig:
                LoadConfiguration(0);
                ReplyN(command, 12);
                // 4 x serial #
                for(int i=0; i<4; ++i) {
                    Port::PutWord(ReadDataFromProgramMemory());
                    IncrementAddress();
                }
                // skip the 2 reserved words
                IncrementAddress();
                IncrementAddress();
                // send the device ID
                Port::PutWord(ReadDataFromProgramMemory());
                IncrementAddress();
                // send the config bits
                Port::PutWord(ReadDataFromProgramMemory());
                break;

            // remove code protection
            case resetConfig:
                FactoryReset();
                Reply0(command);
                break;

            case incrementAddress:
                IncrementAddress();
                Reply0(command);
                break;

            // 'c' write config value
            case writeConfig:
                got = Port::GetWord();      // this should be 2 (= 1 16bit word)
                EnterProgrammingMode();
                LoadConfiguration(Port::GetWord());
                //LoadConfiguration(Port::GetWord() & 0x3f01);
                AddPC(7);
                BeginProgrammingOnlyCycle();
//                WriteWord14(0);
                //BeginProgrammingOnlyCycle();
                EnterProgrammingMode();
                Reply0(command);
                break;

            // 'R' - reset by powering the chip off and on again
            case execute:
                RunPIC();
                Reply0(command);
                break;

            default:
                Reply0(command ^ 0xff);
                break;
        }
    }
}

//////////////////////////////////////////////////////////////////////

void WriteBit(uint8_t bit) {
    delay_Thld0();
    digitalWrite(CLOCK, HIGH);
    digitalWrite(DATA, bit);
    delay_Tset1();
    digitalWrite(CLOCK, LOW);
    delay_Thld1();
}

//////////////////////////////////////////////////////////////////////

byte ReadBit() {
    delay_Thld0();
    digitalWrite(CLOCK, HIGH);
    delay_Tset1();
    byte v = digitalRead(DATA);
    digitalWrite(CLOCK, LOW);
    delay_Thld1();
    return v;
}

//////////////////////////////////////////////////////////////////////
// bits from LSB to MSB from Arduino <-> PIC

void WriteBits(uint16_t data, int n) {
    for(; n > 0; --n) {
        WriteBit(data & 1);
        data >>= 1;
    }
}

//////////////////////////////////////////////////////////////////////

uint16_t ReadBits(int n) {
    uint16_t v = 0;
    uint16_t mask = 1;
    for(int i = 0; i < n; ++i) {
        if(ReadBit()) {
            v |= mask;
        }
        mask <<= 1;
    }
    return v;
}

//////////////////////////////////////////////////////////////////////
// send 14 bits to PIC

void WriteWord14(uint16_t v) {
    WriteBits((v << 1) & 0x7ffe, 16);
}

//////////////////////////////////////////////////////////////////////

uint16_t ReadWord() {
    pinMode(DATA, INPUT);
    uint16_t v = ReadBits(16);
    pinMode(DATA, OUTPUT);
    return (v >> 1) & 0x3fff;
}

//////////////////////////////////////////////////////////////////////

void WriteCmd(uint8_t cmd) {
    WriteBits(cmd, 6);
    delay_Tdly2();
}

//////////////////////////////////////////////////////////////////////
// PC += n

void AddPC(int n)
{
    for(int i=0; i<n; ++i) {
        IncrementAddress();
    }
}

//////////////////////////////////////////////////////////////////////

uint16_t ReadDataFromProgramMemory() {
    WriteCmd(READ_DATA_PROGRAM);
    return ReadWord();
}

//////////////////////////////////////////////////////////////////////

uint16_t ReadDataFromDataMemory() {
    WriteCmd(READ_DATA_DATA);
    return ReadWord();
}

//////////////////////////////////////////////////////////////////////

void LoadDataForProgramMemory(uint16_t v) {
    WriteCmd(LOAD_DATA_PROGRAM);
    WriteWord14(v);
    delay_Tdly2();
}

//////////////////////////////////////////////////////////////////////

void LoadDataForDataMemory(uint16_t v) {
    WriteCmd(LOAD_DATA_DATA);
    WriteWord14(v);
    delay_Tdly2();
}

//////////////////////////////////////////////////////////////////////

void BeginProgrammingOnlyCycle() {
    WriteCmd(BEGIN_PROGRAMMING_ONLY);
    delay_Tprog();
}

//////////////////////////////////////////////////////////////////////

void BeginEraseProgrammingCycle() {
    WriteCmd(BEGIN_ERASE_PROGRAMMING);
    delay_TeraTprog();
}

//////////////////////////////////////////////////////////////////////

void BulkEraseProgramMemory() {
    WriteCmd(BULK_ERASE_PROGRAM);
    delay_TeraTprog();
}

//////////////////////////////////////////////////////////////////////

void BulkEraseDataMemory() {
    WriteCmd(BULK_ERASE_DATA);
    delay_TeraTprog();
}

//////////////////////////////////////////////////////////////////////

#if 1   // A
void FullEraseProgramMemory() {
    LoadDataForProgramMemory(0x3fff);
    BulkEraseProgramMemory();
}
#else
void FullEraseProgramMemory() {
    uint16_t Erase = 0x3fff;
    LoadDataForProgramMemory(Erase);
    WriteCmd(BULK_ERASE_PROGRAM);
    WriteCmd(BEGIN_PROGRAMMING_ONLY);
    delay_Tera();
}
#endif

//////////////////////////////////////////////////////////////////////

void IncrementAddress() {
    WriteCmd(INCREMENT_ADDRESS);
}

//////////////////////////////////////////////////////////////////////

void LoadConfiguration(uint16_t v) {
    WriteCmd(LOAD_CONFIG);
    WriteWord14(v);
}

//////////////////////////////////////////////////////////////////////

// TODO (chs): different flavours for different PICs, this is for 16F628A

void FactoryReset() {
    EnterProgrammingMode();
    LoadConfiguration(0x3fff);
    BulkEraseProgramMemory();
    EnterProgrammingMode();
    BulkEraseDataMemory();
    EnterProgrammingMode();
}