#include "Pins.h"
#include "Port.h"

static int activity;

byte Port::GetByte() {
    digitalWrite(VPP_LED, ((++activity) >> 3) & 1);
    do ; while(Serial.available() == 0);
    return Serial.read();
}

void Port::PutByte(byte b) {
    Serial.write(b);
}

void Port::PutWord(uint16_t v) {
    Serial.write(v & 0xff);
    Serial.write(v >> 8);
}

uint16_t Port::GetWord() {
    uint16_t lsb = (uint16_t)GetByte();
    uint16_t msb = (uint16_t)GetByte();
    return (msb << 8) | lsb;
}

