#ifndef __PORT_H__
#define __PORT_H__

class Port
{
    public:

    //////////////////////////////////////////////////////////////////////
    // Read a byte from the serial port

    static uint8_t GetByte();

    //////////////////////////////////////////////////////////////////////
    // Send a byte

    static void PutByte(uint8_t b);

    //////////////////////////////////////////////////////////////////////
    // Send a 16bit word to the PC on the serial port
    // LSB then MSB from PC <-> Arduino

    static void PutWord(uint16_t v);

    //////////////////////////////////////////////////////////////////////
    // Get a 16bit word from the PC on the serial port

    static uint16_t GetWord();

};

#endif