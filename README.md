# **PICSI** #

## Arduino header for programming PIC16 series microcontrollers. ##

### Folders: ###

**board\\**

The Schematic and PCB. Adding a socket and making it compatible with multiple IC packages would be a good future improvement.

**picpgm\\**

This is the PC utility which talks to the Arduino code. It loads HEX files and sends them to the PICSI etc. It's in C# but would be very simple to port to C++ and make it cross-platform.

**arduino\\**

This is the code which runs on the Arduino. It listens for commands from picpgm and executes them. It does nothing complicated, just drives the ICSP lines.